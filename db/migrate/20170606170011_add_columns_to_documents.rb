class AddColumnsToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :type, :string, :default => 'word'
  	add_column :documents, :print_status, :string, :default => 'waiting'
  	add_column :documents, :transaction_status, :string, :default => 'unpaid'
  	add_column :documents, :price, :decimal, precision: 5, scale: 2, :default => 0
  	add_column :documents, :number_of_copies, :integer, :default => 1
  	add_column :documents, :page_range, :string, :default => 'all'
  	add_column :documents, :orientation, :string, :default => 'portrait'
  	add_column :documents, :color, :string, :default => 'colored'
  	add_column :documents, :slide_layout, :string, :default => 'full-page'
  	add_column :documents, :pages_per_paper, :integer, :default => 1
    change_column :documents, :size, :string, :default => 'short'
  end
end
