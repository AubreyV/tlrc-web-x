class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
    	t.string :user_id, :primary_key 
    	t.string :password
    end

    create_table :documents do |t|
    	t.belongs_to :users, index: true
    	t.string :file_name
      t.string :attachment
      t.string :size
      t.integer :number_of_pages

      t.timestamps null: false
    end
  end
end