class UserController < ApplicationController

	def dashboard
		@announcements = Announcement.all
		@documents = Document.all
	end
end
