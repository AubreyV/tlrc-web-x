class SessionsController < ApplicationController

	def new
		if logged_in?
			redirect_to user_dashboard_path
		end
	end

	def create
		@user = User.find_by(user_id: params[:session][:user_id])
		if @user && @user.password == params[:session][:password]
			log_in @user
			redirect_to user_dashboard_path
		else
			flash.now[:danger] = 'Invalid student number/password!'
			render 'new'
		end
	end
	def destroy
		log_out
		redirect_to login_path
	end
end