class DocumentsController < ApplicationController
  def index
  	@documents = Document.all
  end

  def new
  	@document = Document.new
  end

  def create
  	@document = Document.new(docu_params)
    @document.users_id = current_user.id
    @document.document_type = params[:document][:document_type]
    @document.number_of_copies = params[:document][:number_of_copies]
    @document.page_range = params[:document][:page_range]
    @document.size = params[:document][:size]
    @document.orientation = params[:document][:orientation]
    @document.color = params[:document][:color]
    @document.note = params[:document][:note]
    
  	if @document.save
  		redirect_to user_dashboard_path, notice: "The document #{@document.file_name} has been uploaded."
  	else
  		render "new"
  	end
  end

  def destroy
  	@document = Document.find(params[:id])
  	@document.destroy
  	redirect_to user_dashboard_path, notice: "The document #{@document.file_name} has been destroyed."
  end

  def update
    @document = Document.find(params[:id])
    @user = User.find(@document.users_id)

    if params[:document][:option] == 'print'
      @document.price = params[:document][:price]
      @document.print_status = 'printed'
      @user.accountability += @document.price

    elsif params[:document][:option] == 'transaction'     
      if params[:document][:transaction_status] == 'unpaid'
        @document.transaction_status = 'paid'
        @user.accountability -= @document.price
      else
        @document.transaction_status = 'unpaid'
        @user.accountability += @document.price
      end
    end

    @document.save
    @user.save
    redirect_to user_dashboard_path
  end
  
  private
  	def docu_params
  		params.require(:document).permit(:file_name, :attachment, :number_of_pages, :size, :document_type, :number_of_copies, :page_range, :orientation, :color)
  	end
end