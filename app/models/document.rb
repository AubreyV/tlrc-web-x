class Document < ActiveRecord::Base
	belongs_to :users
	mount_uploader :attachment, AttachmentUploader
	validates :file_name, presence: true
end
